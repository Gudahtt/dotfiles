export PATH="${HOME}/bin:${HOME}/.poetry/bin:${PATH}"

for file in "${HOME}"/.dotfiles/{prompt,aliases,exports,options,misc}.bash; do
	[ -r "${file}" ] && [ -f "${file}" ] && source "${file}"
done
unset file

