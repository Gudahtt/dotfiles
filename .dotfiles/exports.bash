#!/bin/bash

# don't put duplicate lines or lines starting with space in the history.
export HISTCONTROL='ignoreboth'

# Increase Bash history size. Allow 32³ entries; the default is 500.
export HISTSIZE='32768'
export HISTFILESIZE="${HISTSIZE}"

# Ingore uninteresting commands
export HISTIGNORE='ls:pwd:bg:fg:history:'

# Record timestamp of each command
export HISTTIMEFORMAT='[%F %T]: '

# Record bash history after each command
export PROMPT_COMMAND='history -a'

# Docker for Windows
export DOCKER_HOST='tcp://localhost:2375'

