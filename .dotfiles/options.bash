#!/bin/bash

# append to the history file, don't overwrite it
shopt -s histappend

# Adjust commands to fit on one line
shopt -s cmdhist

# Preserve newlines in multi-line commands
shopt -s lithist

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar

