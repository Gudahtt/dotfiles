#!/usr/bin/bash

prompt_git() {
	local s='';
	local branchName='';

	# Check if the current directory is in a Git repository.
	if [[ "$(git rev-parse --is-inside-work-tree 2>/dev/null)" != "true" ]]; then
		return
	fi
	# Ensure the index is up to date.
	git update-index --really-refresh -q &>/dev/null;

	# Check for uncommitted changes in the index.
	if ! git diff --quiet --ignore-submodules --cached; then
		s+='+';
	fi;

	# Check for unstaged changes.
	if ! git diff-files --quiet --ignore-submodules --; then
		s+='!';
	fi;

	# Check for untracked files.
	if [ -n "$(git ls-files --others --exclude-standard)" ]; then
		s+='?';
	fi;

	# Get the short symbolic ref.
	# If HEAD isn’t a symbolic ref, get the short SHA for the latest commit
	# Otherwise, just give up.
	branchName="$(git symbolic-ref --quiet --short HEAD 2> /dev/null || \
		git rev-parse --short HEAD 2> /dev/null || \
		echo '(unknown)')";

	[ -n "${s}" ] && s=" [${s}]";

	echo -e "${1}${branchName}${2}${s}";
}

build_prompt() {
	local exit_status="$?"
	local status="${1}"; shift
	local git_branch_style="${1}"; shift
	local git_status_style="${1}"; shift
	local good_prompt="${1}"; shift
	local bad_prompt="${1}"; shift

	local prompt;
	if [[ $exit_status == 0 ]]; then
		prompt="${good_prompt}"
	else
		prompt="${bad_prompt}"
	fi

	local git_info
	git_info="$(prompt_git "${git_branch_style}" "${git_status_style}")"

	echo -e "${status}${git_info}\n${prompt}"
}

if tput setaf 1 &> /dev/null; then
	tput sgr0; # reset colors
	bold=$(tput bold);
	reset=$(tput sgr0);
	# Solarized colors, taken from http://git.io/solarized-colors.
#	black=$(tput setaf 0);
	blue=$(tput setaf 33);
#	cyan=$(tput setaf 37);
	green=$(tput setaf 64);
	orange=$(tput setaf 166);
#	purple=$(tput setaf 125);
	red=$(tput setaf 124);
	violet=$(tput setaf 61);
	white=$(tput setaf 15);
	yellow=$(tput setaf 136);
else
	bold='';
	reset="\e[0m";
#	black="\e[1;30m";
	blue="\e[1;34m";
#	cyan="\e[1;36m";
	green="\e[1;32m";
	orange="\e[1;33m";
#	purple="\e[1;35m";
	red="\e[1;31m";
	violet="\e[1;35m";
	white="\e[1;37m";
	yellow="\e[1;33m";
fi;

# Highlight the user name when logged in as root.
if [[ "${USER}" == "root" ]]; then
	user_style="${red}";
else
	user_style="${orange}";
fi;

# Highlight the hostname when connected via SSH.
if [[ -n "${SSH_TTY}" ]]; then
	host_style="${bold}${red}";
else
	host_style="${yellow}";
fi;

# Set the terminal title and prompt.
PS1="\[\033]0;\W\007\]"; # working directory base name
PS1+="\[${bold}\]\n"; # newline
PS1+="\$(build_prompt \"\[${user_style}\]\u\[${white}\] at \[${host_style}\]\h\[${white}\] in \[${green}\]\w\" \"\[${white}\] on \[${violet}\]\" \"\[${blue}\]\" \"\[${white}\]\$ \" \"\[${red}\]\$ \")"
PS1+="\[${reset}\]"; # reset color
export PS1;

PS2="\[${yellow}\]→ \[${reset}\]";
export PS2;

