#!/bin.bash

alias l='ls -lF --color=auto'
alias la='ls -lFA --color=auto'
alias grep='grep --color=auto'
alias dotfiles='/usr/bin/git --git-dir="${HOME}/.dotfiles-repo" --work-tree="${HOME}"'

alias cdev='cd /c/Development/git'
alias time="$(which time) -f '\t%E real,\t%U user,\t%S sys,\t%K kb avg mem,\t%M kb max mem'"

