#!/usr/bin/env bash
set -o pipefail

function die {
	local message="${1}"

	printf "ERROR: %s\\n" "${message}" >&2

	exit 1
}

function main {
	local backup_dir
	local file_dir
	local file
	local git_file_info
	backup_dir="${HOME}/.dotfiles-backup"

	if [[ ! -d "${backup_dir}" ]]; then
		mkdir "${backup_dir}" || die 'Failed to create backup directory'
	fi
	pushd "$HOME" > /dev/null
		git --git-dir="$HOME/.dotfiles-repo" --work-tree="$HOME" ls-tree --full-tree -r HEAD | awk '{ print $4 }' | while read -r file; do
			if [[ -e "$file" ]]; then
				file_dir="$(dirname "${file}")" || die "Failed to get dirname for ${file}"
				if [[ ! -d "${backup_dir}/${file_dir}" ]]; then
					mkdir -p "${backup_dir}/${file_dir}" || die "Failed to create directory ${backup_dir}/${file_dir}"
				fi
				printf "Backing up %s\n" "${file}"
				mv "$file" "${backup_dir}/${file}" || die "Failed to copy file ${file}"
			fi
		done || die "Loop failed"
	popd > /dev/null
}

main

