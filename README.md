Dotfiles
==========

These dotfiles were derived from a number of publicly available dotfiles repos; I can't recall which ones.

They are meant to be used with a bare git repo, which is a pattern I learned about here: https://harfangk.github.io/2016/09/18/manage-dotfiles-with-a-git-bare-repository.html

To clone this dotfiles repo, run the command: `git clone --bare [REPO_URL] $HOME/.dotfiles-repo`
From then on, you can interact with the repo using the command `dotfiles` in place of `git`, e.g. `dotfiles fetch`. The `dotfiles` alias can be found in `.dotfiles/aliases.bash`.

